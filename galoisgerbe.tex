\documentclass{shortnotes}

%%
\usepackage{style-shortnotes}
%%
\usepackage{custommath_gen}
\usepackage{category-names}
\usepackage{custommath_font}
\usepackage{custommath_gerbe}



\usepackage{gitinfo2}
%%
\title{\normalsize{Galois gerbes and $\erig$}}
\date{}
\author{\normalsize{Aaron Wild}\footnote{\url{aaron.wild@posteo.net}. This is version \gitAbbrevHash{} of this document. An up-to-date version can be found at \url{aaronwild.gitlab.io/writing}}}
\abstracttext{We outline the construction of Kaletha's Galois gerbe $\erig$ that is used for the parametrization of rigid inner forms, following \cite{kaletha}.
These notes were prepared for the gradutate seminar on the local Langlands conjecturs for non quasi-split groups, which took place in Bonn during the summer semester 2023. 
I would like to thank Zhen Huang, Han Jiadong, and David Schwein for helping me prepare for this talk.
Some claims and questions I could not resolved are marked in {\color{todocolor} pink}, additions for clarification after the talk in \latercol{blue}.}
\bibliography{lit.bib}
\begin{document}
\maketitlepage
\thispagestyle{empty}
\vspace{.5cm}
Throughout, $F$ denotes a local field of characteristic zero.
\section{Recollections}
\begin{numtext}
  One of the main features of Galois cohomology is Tate-Nakayama duality, which can be understood as an isomorphism
  \[
    \hh^1(\Gamma,T)
    = 
    \hht^1(F,T)
    \isomorphism 
    \hht^{-1}(E/F,\cocha(T))
    =
    \cocha(T)_{\Gamma}\tors,
  \]
  where $T$ is a torus, $E/F$ a finite Galois extension that splits $T$, and $\cocha(T)_{\Gamma}\tors$ is the torsion part of the Galois coinvariants of the action on the cocharacters $\cocha(T)$.
  Kottwitz gives an interpretation of the right-hand side in terms of the dual group, and obtains an isomorphism\footnote{An expository account of this can be found in \cite{drin-kott}.} 
  \[
    \hh^1(\Gamma,T) 
    \isomorphism 
    \pi_0(\widehat{T}^\Gamma)^D.
  \]
  This morphism in fact extends to all reductive groups:
  \begin{inthm}[{\cite[Thm.1.2]{kottwitz-elliptic}}]
    Let $G$ be a connected reductive group over a local field $F$ of characteristic zero.
    Then there is a unique morphism  
    \[
      \alpha\mc
      \hh^1(\Gamma,G)
      \longrightarrow
      \pi_0(\cen(\hat{G}))^D,
    \]  
    that extends the Tate-Nakayama isomorphism, in the sense that for any maximal torus $T$ of $G$, the diagram 
    \[
      \begin{tikzcd}
        \hh^1(\Gamma,T)
        \ar{r}
        \ar{d}
        &
        \hh^1(\Gamma,G)
        \ar{d}
        \\ 
        \pi_0(\hat{T}^\Gamma)^D 
        \ar{r}
        &
        \pi_0(\cen(\hat{G})^\Gamma)^D 
      \end{tikzcd}
    \]
    commutes.
    If $F$ is $p$-adic, then this is an isomorphism.
    If $F = \rr$, then the kernel and image can be explicitly described.
  \end{inthm}
\end{numtext}
\begin{numtext}
  This is helpful because we can use it to calculate the Galois cohomology groups that classify different variants of ``inner twists'':
  Recall that for a reductive algebraic group $G$, we have the bijection 
  \[
    \begin{tikzcd}
      \Set{\text{inner twists $\xi\mc G \to G'$}}/_{\text{iso. of inner twists}}
      \ar{d}{\sim}
      \\
      \hh^1(\Gamma, \adj{G}).
    \end{tikzcd}
  \]
  We also saw that we have 
  \[
    \begin{tikzcd}
      \Set{\text{pure inner twists $(\xi\mc G \to G',z)$}}/_{\text{iso. of pure inner twists}}
      \ar{d}{\sim}
      \\
      \hh^1(\Gamma, G),
    \end{tikzcd}
  \]
  and that these notions are compatible in the sense that the diagram 
  \[
    \begin{tikzcd}[column sep = huge]
      \Set{\text{pure inner twists of $G$}}/_{\sim}
      \ar{r}[above]{(\xi,G',z) \mapsto (\xi,G')}
      \ar{d}[left]{\sim}
      &
      \Set{\text{inner twists of $G$}}/_{\sim}
      \ar{d}[right]{\sim}
      \\ 
      \hh^1(\Gamma,G)
      \ar{r}      & 
      \hh^1(\Gamma,\adj{G})
    \end{tikzcd}
  \]
  commutes.
  However, the lower horizontal arrow is not necessarily surjective or injective:
\end{numtext}

\begin{example}
  \label{example:special1}
  Consider the case of $G = \SL_n$ over $\qqp$.
  We have an explicit description of the inner forms of $G$ via central simple $F$-algebras of $F$-dimension $n^2$ every inner form of $\SL_n(\qqp)$ is of the form $\der{\GL_m(D)}$, where $D$ is is a division algebra over $F$ of dimension $d^2$ and $n = md$ holds.
  Now the dual group of $\adj{\SL_n(F)}$ is given by $\widehat{\adj{\SL_n(\qqp)}} = \SL_n(\cc)$.\footnote{We know that taking $\widehat{(-)}$ interchanges being of adjoint and of simply-connected type, and we have $\widehat{\SL_n(\qqp)} = \PGL_n(\cc) = \PSL_n(\cc)$ (since $\cc$ is algebraically closed), which clearly admits the simply-connected cover $\SL_n(\cc)$.}
  We can then also use the Kottwitz isomorphism to calculate both $\hh^1(\Gamma,G)$ and $\hh^1(\Gamma,\adj{G})$:
  \begin{itemize}
    \item 
      For $G$, we have $\hh^1(\Gamma,G) = \pi_0(\cen(\widehat{G})^{\Gamma})^D = \pi_0(\cen(\PGL_n(\cc))^{\Gamma})^D = \Set{1}$, since $\PGL_n(\cc)$ has trivial center.
      \emph{We see that the only pure inner twist of $\SL_n$ is the trivial pure inner twist!}
    \item 
      For $\adj{G}$, we have 
      \[
        \hh^1(\Gamma,\adj{G}) = \pi_0(\cen(\widehat{\adj{G}})^{\Gamma})^D = \pi_0(\cen(\SL_n(\cc))^{\Gamma})^D = (\mu_n)^D,
      \]
      since the Galois action is trivial.
  \end{itemize}
\end{example}
In fact, we always encounter this problem when $G$ is $p$-adic and simply connected, as then $\hh^1(\Gamma,G) = \Set{1}$.

\begin{example}
  \label{example:unitary1}
 Consider the case of $\uni$ over $\qqp$ for $n$ odd.
 Then the natural map $\hh^1(\Gamma,\uni) \to \hh^1(\Gamma,\mathrm{U}^{\ast}_{n,\mathrm{adj}})$ is given by the projection $\zz/2\zz \to \Set{1}$, i.e. $\uni$ does not have a non-trivial inner form, but there are two non-equivalent ways to view $\uni$ as an pure inner form of itself.
\end{example}
\begin{proof}
  We claim that in the case $n$ odd, we have 
  \[
    \hh^1(\Gamma,\uni) 
    = 
    \zz/2\zz 
    ~\text{and}~
    \hh^1(\Gamma,\uni*)
    =
    \Set{1}.
  \]
  Again, we can use the Kottwitz isomorphism
  \[
    \hh^1(\Gamma,G) = \pi_0(\cen(\hat{G})^{\Gamma})^{D} 
  \]
  in both cases (where $\hat{G}$ is the Langlands dual group).
  Now, we have $ \widehat{\uni} = \GL_n(\cc)$, and the Galois action factors over $\Gamma_{E/F}$, where the non-trivial element $\sigma$ acts via $g \mapsto \Adj(J_n)g^{-1,t}$, for 
  \[ 
     J_n 
     = 
     \begin{bmatrix}
       &
       &
       &
       &
       -1 
       \\
       &
       &
       &
       1
       &
       \\  
       &
       &
       -1
       &
       &
       \\  
       &
       \reflectbox{$\ddots$}
       &
       &
       &
       \\  
       (-1)^n
       &
       &
       &
       &
     \end{bmatrix}
  \]
  Moreover, we have $\uni* = \uni/\mathrm{U}^{\ast}_1$, and $\widehat{\uni*} = \SL_n(\cc)$, with the same action.
  \par 
  Now the restriction of the Galois action to $\cen(\GL_n(\cc)) = \cc^{\times}$ and $\cen(\SL_n(\cc)) = \mu_n$ is in both cases given by complex conjugation.
  So $\hh^1(\Gamma,\uni) = \pi_0(\cc^{\times,(-)^{-1}})^D \cong \zz/2\zz$ (since we can restrict to the action on $S^1$, where it is given by complex conjugation), and $\hh^1(\Gamma,\uni*) \cong \Set{1}$ (since $n$ is odd).
\end{proof}
\section{Galois gerbes in characteristic $0$}
\begin{defn}[{\cite{undgerben},\cite{allfields}}]
  Assume $\rchar(F) = 0$.
  A \emph{Galois gerbe} is a group extension 
  \[
    \begin{tikzcd}[column sep = small]
      1 
      \ar{r}
      &
      u(\bar{F})
      \ar{r}
      &
      \mathcal{E}
      \ar{r}
      &
      \Gamma 
      \ar{r}
      &
      1, 
    \end{tikzcd}
  \]
  where $u(\bar{F})$ is the $\bar{F}$-valued points of an abelian $F$-group scheme $u$, 
  \latercol{
    and such that the action of $\Gamma$ on $u(\bar{F})$ is given as
    \[
      \sigma\cdot a = \hat{\sigma}a\hat{\sigma}^{-1}
    \]
    with $\hat{\sigma} \in \she$ a preimage of $\sigma$.
  }
  We call the group $u(\bar{F})$ the \emph{band} of the gerbe.
  \latercol{
    Two such extensions are called equivalent if there is a commutative diagram of the form}
    \[
      \color{RoyalBlue}
      \begin{tikzcd}
        1 
        \ar{r}
        &
        u(\bar{F})
        \ar{r}
        \ar[equal]{d}
        & 
        \she' 
        \ar{r}
        \ar{d}[right]{\sim}
        &
        \Gamma 
        \ar[equal]{d}
        \ar{r}
        &
        1
        \\ 
        1 
        \ar{r}
        &
        u(\bar{F})
        \ar{r}
        &
        \she 
        \ar{r}
        &
        \Gamma 
        \ar{r}
        &
        1
      \end{tikzcd}
    \]
\end{defn}

\begin{defn}
  Let $G$ be an algebraic group defined over $F$, and $Z \to G$ with $Z$ a finite multiplicative group whose image is contained in the center of $G$.
  Let $u \to \she$ be a Galois gerbe.
  The set $G(\bar{F})$ carries a continous $\Gamma$-action, which can be inflated to a continous $\she$-action.\footnote{i.e. $\gamma \in \she$ acts by via its image in $\Gamma$}
  Let 
  \[
    \cocyc^1(u \to \she, Z \to G) \sse \cocyc^1_{\mathrm{cont}}(\she,G(\bar{F}))
  \]
  consist of these continous cocycles $f\mc \she \to G(\bar{F})$ such that their restriction to $u(\bar{F}) \sse \erig$ factors as 
  \[
    \begin{tikzcd}
      u(\bar{F})
      \ar{r}[above]{\snake{f}(\bar{F})}
      \ar[hookrightarrow]{d}
      &
      Z(\bar{F})
      \ar[hookrightarrow]{d}
      \\ 
      \she
      \ar{r}[below]{f}
      &
      G(\bar{F})
    \end{tikzcd}
  \]
  where $\snake{f}(\bar{F})$ is the map on $\bar{F}$-valued points induced by a morphism of group schemes $\bar{f}\mc u \to Z$.
  Now $\cocyc(u \to \she, Z \to G)$ is closed under the equivalence relation of being a 1-coboundary, and so we can define 
  \[
    \hka*
    \defo 
    \cocyc(u \to \she, Z \to G)
    / 
    \bdry(\she, G(\bar{F})),
  \]
  which is naturally a pointed set.
\end{defn}

\begin{lem}
  Let $G$ be an algebraic group, and $u\to\she$ a Galois gerbe.
  Then the canonical map $\hh^1(\Gamma,G) \to \hh^1(\Gamma,\adj{G})$ factors as 
  \[
    \begin{tikzcd}[column sep = small]
      \hh^1(\Gamma,G)
      \ar{rr}
      \ar[hookrightarrow]{rd}
      &
      &
      \hh^1(\Gamma,\adj{G})
      \\ 
      &
      \hh^1(u\to \she, Z \to G)
      \ar{ur}
      &
    \end{tikzcd}
  \]
  for any finite central subgroup $Z \to G$.
\end{lem}

So the cohomology set $\hh^1(u\to \she, Z \to G)$ is a good candidate for a set that is ``big enough'' for the map to $\hh^1(\Gamma,\adj{G})$ to be surjective.

\begin{prop}[{\cite[(2.7.7)]{nsw}}]
  Then there is a bijection between Galois gerbes 
  \[
    \begin{tikzcd}[column sep = small]
      1 
      \ar{r}
      &
      u(\bar{F})
      \ar{r}
      &
      \she 
      \ar{r}
      &
      \Gamma 
      \ar{r}
      &
      1 
    \end{tikzcd}
  \]
  and the set $\hh^2(\Gamma,u)$.
\end{prop}

\begin{proof}[Sketch of proof]
  \latercol{
    Let us sketch the proof in the case that $u(\bar{F})$ is finite:
    \begin{itemize}
      \item 
        Suppose we are given such an extension.
        The map $\she \to \Gamma$ admits a continous section $s\mc \Gamma \to \she$, which is not necessarily a group homomorphism (this is the case if and only if the extension splits), but we can still assume $s(1) = 1$.
        Consider now the map 
        \[
          \phi_s\mc \Gamma^2 \to \she,~ (g_1,g_2) \mapsto s(g_1)s(g_2)s(g_1g_2)^{-1} .
        \]
        This map becomes the constant map after composing with the projection $\she \to \Gamma$, so factors over $u(\bar{F}) \sse \she$, i.e. we obtain a continous 2-cochain.
        One can then check that this map is in fact a 2-cocycle, and that for any other choice of section $s'$ the associated map $\phi_{s'}$ is cohomologous to $\phi_s$, so the extension gives a well-defined element of $\hh^2(\Gamma,u)$.
        One then further checks that two equivalent extensions give rise to the same cohomology class.
      \item 
        For the other direction, let $\phi \mc \Gamma \times \Gamma \to u(\bar{F})$ be a continous 2-cocycle.
        We can assume that $\phi$ is ``normalized'', i.e. that $\phi(1,g) = \phi(g,1) = 1$ holds for all $g\in \Gamma$ (more precisesly, the cohomology class of $\phi \in \hh^2(u,\Gamma)$ contains a normalized representative).
        We then use $\phi$ to define a group structure on the topological space $u(\bar{F}) \times \Gamma$ (endowed with the product topology), via 
        \[
          (u_1,g_1)
          \star_{\phi}
          (u_2,g_2)
          \defo 
          (u_1 \cdot g_1u_2 \cdot \phi(g_1,g_2), g_1g_2),
        \]
        where we use the explicit description of the $\Gamma$-action on $u(\bar{F})$ to make sense of the factor $g_1u_2$.
        One then uses the cocycle property and that $\phi$ is normalized to calculate that this defines a group structure on $u(\bar{F})\times \Gamma$. 
        One then further verifies that the canonical inclusion and projection become group homomorphisms, and that any cohomologous normalized cocycle yields an equivalent extension.
    \end{itemize}
  }
\end{proof}

\latercol{
  \begin{rem}
    We can also describe the group $\hh^1(u,\Gamma)$ in this setup.
    Namely, for any extension 
    \[
        1 
        \to
        u(\bar{F})
        \to
        \she  
        \xrightarrow{\pi}
        \Gamma 
        \to
        1,
    \]
    we can consider the group $\auto([\she])$ of automorphisms of the extension.
    Say that two such automorphisms $f_1,f_2$ are equivalent if $f_1 = \gamma_u\circ f_2$ holds, where $\gamma_m \mc \she \smalliso \she$ is given by conjugation with some element in $u(\bar{F})$, and consider the quotient $\auto([\she])/\sim$ by this equivalence relation.
    One can then check that for any continous cocycle $\phi \mc \Gamma \to u(\bar{F})$ and $f\in \auto([\she])/\sim$, the morphism 
    \[
      \she \to \she,~ x\mapsto \phi(\pi(x)) \cdot f(x)
    \]
    is again an element of $\auto([\she])$, and that this descends to a well-defined action of $\hh^1(u,\Gamma)$ on $\auto([\she])/\sim$, that is moreover simply transitive.
  \end{rem}
}
The idea now is to find such a $u$ and a ``distinguished'' element in $\hh^2(\Gamma,u)$ to parametrize inner forms.
Note that in the trivial case $u=\Set{1}$, we just recover ordinary Galois cohomology.
\begin{numtext}
  Before we continue, let us make the general observation that for any $G$ algebraic, the long-exact cohomology sequence for $\begin{tikzcd}[column sep = small, cramped] \cen(G) \ar[hookrightarrow]{r} & G \ar[twoheadrightarrow]{r} & \adj{G} \end{tikzcd}$ gives rise to the long-exact sequence of pointed sets
  \[
    \begin{tikzcd}[column sep = small]
      \hh^1(\Gamma, G)
      \ar{r} 
      &
      \hh^1(\Gamma, \adj{G}) 
      \ar{r}[above]{\delta}
      &
      \hh^2(\Gamma,\cen(G)),
    \end{tikzcd}
  \]
  and so for a class $\eepsilon \in \hh^1(\Gamma,\adj{G})$, to it lying in the image of $\hh^1(\Gamma,G) \to \hh^1(\Gamma,\adj{G})$ is given by the class $\delta(\eepsilon) \in \hh^2(\Gamma,\cen(G))$.
  The point now is that $\delta(\eepsilon)$ defines a Galois gerbe $\she^{\eepsilon}$ banded by $\cen(G)$, and inflation is compatible with this in the sense that the diagram 
  \[
    \begin{tikzcd}[column sep = small]
      \hh^1(\she^{\eepsilon},G)
      \ar{r}
      &
      \hh^1(\she^{\eepsilon},\adj{G})
      \ar{r}
      &
      \hh^2(\she^{\eepsilon},\cen(G))
      \\ 
      \hh^1(\Gamma,G)
      \ar{r}
      \ar{u}[left]{\infl}
      &
      \hh^1(\Gamma,\adj{G})
      \ar{r}
      \ar{u}[left]{\infl}
      &
      \hh^2(\Gamma,\cen(G))
      \ar{u}[right]{\infl}
    \end{tikzcd}
  \]
  commutes.\footnote{\todocol{I did not check this for non-abelian cohomology, but that it holds in the abelian case is \cite[(1.5.2)]{nsw}.}}
  The point now is that $\inf(\delta(\eepsilon)) = 0$ holds in $\hh^2(\she^{\eepsilon},\cen(G))$ (\cite[VIII.5]{giraud}).
  In other words, passing from $\Gamma$ to $\she^{\eepsilon}$ alows us to at least have $\eepsilon$ in the image of the map $\hh^1(\she^{\eepsilon},G) \to \hh^1(\she^{\eepsilon},\adj{G})$.
  In particular, if $\hh^2(\Gamma,\cen(G))$ is cyclic (eg. $G = \SL_n$), then passing to the gerbe defined by a generator already makes the map $\hh^1(\she,G) \to \hh^1(\she,\adj{G})$ surjective.
\end{numtext}

\section{Construction of $\hka*$} 
\begin{construction}
  Let $F$ be a local field of characteristic zero, with fixed algebraic closure $\bar{F}$.
  Let $R_{E/F}[n] \defo \res[E/F] \mu_n$ be the Weil restriction of scalars of the group $\mu_n$ of $n$-th roots of unity.
  Explicitly, if $K / F$ is a Galois extension, we have 
  \[
    R_{E/F}[n](K)
    =
    \hom(\Gamma_{E/F},\mu_n(\bar{F}))^{\Gamma_K},
  \]
  where the Galois action is given by ``conjugation'' in the sense of 
  \[
    (\sigma\cdot f)(\tau)
    \defo 
    \sigma(f(\sigma^{-1} \tau)),
  \]
  for $f\mc \Gamma_{E/F} \to \mu_n(\bar{F})$, $\sigma \in \Gamma_K$ and $\tau \in \Gamma_{E/F}$.
  We have a natural ``diagonal'' inclusion $\mu_n \hookrightarrow R_{E/F}[n]$, given on $K$-valued points by mapping $x\in \mun(K)$ to the the constant map with value $x \in \mun(\bar{F})$ (as we have, by assumption, $\mun(K) \sse \mun(\bar{F})$).
  Let $u_{E/F,n}$ be the cokernel of this map, so that we have an exact sequence of commutative group schemes 
  \[
    \begin{tikzcd}[column sep = small]
      1 
      \ar{r}
      &
      \mun 
      \ar{r}
      &
      R_{E/F,n}[n]
      \ar{r}
      &
      u_{E/F,n}
      \ar{r}
      &
      1
    \end{tikzcd}
  \]      
  For every tower of Galois extension $F \sse K \sse E$ and $m \in \nn$ a multiple of $n$, we have a natural map $p \mc R_{K/F}[m] \to R_{E/F}[n]$, which can be described on points as 
  \[
    (pf)(a)
    =
    \prod_{\substack{b\in \Gamma_{K/F}\\ b\to a}} f(b)^{m/n}.
  \]
  This is a surjection, and induces a surjection $u_{K/F,m} \twoheadrightarrow u_{E/F,n}$.
  Let
  \begin{equation}
    \label{udef}
    u \defo \varprojlim u_{E/F,n}
  \end{equation}
  be the inverse limit over the index category $\mathcal{I}$ consisting of pair $(E/F,n)$ with $E/F$ Galois, $n\in \nn$, and 
  \[
    \maps_{\mathcal{I}}
    ((K/F,m),(E/F,n))
    =
    \begin{cases}
      \Set{\ast},
      &
      \text{if $E\sse K$ and $n\divd m$;}
      \\ 
      \emptyset
      &
      \text{otherwise.}
    \end{cases}
  \]
  Note that the $\bar{F}$-points of $u$ have a natural profinite topology.\footnote{Since we have $u(\bar{F}) = \varprojlim u_{E/F,n}(\bar{F})$, since taking global sections commutes with limits (it's a right-adjoint), and each of the $u_{E/F,n}(\bar{F})$ is endowed with the discrete topology}
  Moreover, $u(\bar{F})$ has a natural continous $\Gamma$-action, induced from the natural $\Gamma$-action on $R_{E/F,n}(\bar{F})$ by post-composition.
  Since $u$ is abelian, we can thus talk of the continous cohomology groups $\hh^i(\Gamma,u) \defo \hh^i(\Gamma,u(\bar{F}))$.
\end{construction}

\begin{theorem}
  We have 
  \[
    \hh^1(\Gamma,u) 
    =
    0 
    ~\text{and}~
    \hh^2(\Gamma,u)
    = 
    \begin{cases}
      \hat{\zz},
      &
      \text{if $F$ is non-archimidean;}
      \\ 
      \zz/2\zz,
      &
      \text{if $F = \rr$.}
    \end{cases}
  \]
\end{theorem}

\begin{proof}
  For $F$ any non-archimidean local field, we have in fact 
  \[
    \hh^1(\Gamma,u) 
    =
    \varprojlim_{E/F, n \geq 1} \hh^1(\Gamma,\res_{E/F} \mu_n)
    \text{~and~}
    \hh^2(\Gamma,u)
    = 
    \varprojlim_{E/F, n \geq 1} \hh^2(\Gamma,\res_{E/F} \mu_n),
  \]
  c.f. \cite[Prop.3.1]{dill}.
  Let us show why this holds and how this implies the claim in the case that $F$ is $p$-adic (this is also the proof in \cite[5.1]{farg-kal}):
  First, we have an exact sequence of the form \cite[\stackstag{07KY}]{stacks}
  \[
    \begin{tikzcd}[column sep = small]
      0 
      \ar{r}
      &
      \displaystyle\rlim_{E/F, n \geq 1} \hh^1(\Gamma, \res_{E/F} \mu_n)
      \ar{r}
      &
      \hh^2(\Gamma,u)
      \ar{r}
      &
      \displaystyle\varprojlim_{E/F, n \geq 1} \hh^1(\Gamma, \res_{E/F} \mu_n)
      \ar{r}
      &
      0
    \end{tikzcd}
  \]
  Now we have $\hh^1(\Gamma,\res_{E/F} \mu_n) = E^\times/E^{\times,n}$ by Kummer theory, which is finite, so in particular, the $\rlim$-term above vanishes, and we have 
  \[
    \hh^2(\Gamma,u) \isomorphism \varprojlim_{E/F,n \geq 1} \hh^2(\Gamma,\res_{E/F} \mu_n).
  \]
  The groups on the right-hand side become acessible via class-field theory: we have 
  \[
    \hh^2(\Gamma,\res_{E/F} \mu_n) 
    = 
    \brau(E)[n] 
    =
    \frac{1}{n} \zz / \zz,
  \]
  and the transition maps are given by multiplication, i.e. for $(E',m)$ and $(E,n)$ in the indexing category, the transition map is given by 
  \[
    \begin{tikzcd}
      \brau(E')[m]
      = 
      \frac{1}{m}\zz / \zz 
      \ar{r}[above]{\cdot m/n}
      &
      \frac{1}{n}\zz / \zz 
      =
      \brau(E)[n].
    \end{tikzcd}
  \]
  This gives $\hh^2(\Gamma,u) = \hat{\zz}$ canonically, as desired.

%  First, we observe that we can take the limit in \eqref{udef} over any co-final subcategory $\mathcal{J}$ of $\mathcal{I}$.
%  Consider $\mathcal{J}$ given by a collection $\Set{(E_k,n_k)}$, where $F = E_0 \subset E_1 \subset E_2 \subset \ldots \subset \bar{F}$ is a tower of field extensions with $\bigcup_k E_k = \bar{F}$, and $\Set{n_k}$ a co-final sequence in $\nn$.
%  As usual for continous cohomology of profinite groups, we then have a natural isomorphism 
%  \begin{equation}
%    \label{u-coh-limit}
%    \hh^i(\Gamma,u)
%    \isomorphism 
%    \varprojlim_{\mathcal{J}}
%    \hh^i(\Gamma,u_k), 
%  \end{equation}
%  with $u_k \defo u_{E_k / F_k, n_k}$.
%  \par 
%  For the calculation of $\hh^2(\Gamma,u)$, we use the following formulation of Tate duality:
%  \begin{inthm}
%    Let $A$ be a finite $\Gamma$-module, and consider its group of characters 
%    \[
%      \cha(A) 
%      \defo 
%      \hom(A,\mu)
%      = 
%      \hom(A,\gmult),
%    \]
%    where $\mu = \bigcup_n \mu_n$.
%    Then for $0 \leq i \leq 2$, the cup-product 
%    \[
%      \hh^i(\Gamma,A) 
%      \times 
%      \hh^{2-i}(\Gamma,\cha(A))
%      \to 
%      \hh^2(\Gamma,\mu)
%      = 
%      \rat / \zz 
%    \]
%    gives a duality betweem the finite groups $\hh^i(\Gamma,A)$ and $\hh^{2-i}(k,\mu)$.
%  \end{inthm}
%  Applying this to the finite $\Gamma$-module $u_k$ gives 
%  \[
%    \hh^2(\Gamma,u_k)
%    \cong 
%    \hh^0(\Gamma,\cha(u_k))^\ast 
%    \cong 
%    \zz / (n_k,[E_k:F])\zz ,
%  \]
%  compatible with the transition maps.
%  Passing to the limit in \eqref{u-coh-limit} gives the desired description of $\hh^2$ in both cases.
\end{proof}

\begin{defn}
  Let $\xi = -1 \in \hat{\zz} = \hh^2(\Gamma,u)$.
  Then this corresponds to a unique extension of profinite groups 
  \begin{equation}
    \label{erig-def}
    \begin{tikzcd}[column sep = small]
      1 
      \ar{r}
      &
      u(\bar{F})
      \ar{r}
      &
      \erig 
      \ar{r}
      &
      \Gamma 
      \ar{r}
      &
      1 
    \end{tikzcd}.
  \end{equation}
  We call $\erig$ the \emph{Kaletha gerbe}.\footnote{In the original paper, the notation $W$ is used instead of $\erig$. We opted for $\erig$ because it avoids confusion with the Weil group, and also because it seems to be better suited for the comparison with the gerbe $\she^{\mathrm{iso}}$ that parametrizes extended pure inner forms which we will see in the next talk.}
\end{defn}
Gainig a better understanding of $\hka*$ is the main objective of the rest of this talk.

\subsection*{Basic properties of $\hka*$}

\begin{lem}
  \label{lem:inf-res}
  Let $Z\to G$ be as before, with $G$ algebraic and $Z$ finite central.
  The inflation-restriction sequence associated to \eqref{erig-def} induces an inflation-restriction sequence for $\hka*$,\footnote{\latercol{Recall that for a profinite group $\Gamma$ with normal subgroup $N \sse G$ and $A$ a $G$-module, there is always the inflation-restriction-sequence 
  \[
    \begin{tikzcd}[column sep = small, ampersand replacement = \&]
      0 
      \ar{r}
      \& 
      \hh^1(\Gamma/N,A^N)
      \ar{r}
      \&
      \hh^1(\Gamma,A)
      \ar{r}
      \&
      \hh^1(N,A)^{\Gamma/N}
      \ar{r}
      \& 
      \hh^2(\Gamma/N,A^N),
    \end{tikzcd}
  \]
  where we disregard the last term if $A$ is not abelian.
  For $A$ abelian, this is part of the five-term sequence associated to the Hochschild-Serre spectral sequence $\mathrm{E}_2^{i,j} = \hh^i(\Gamma/N,\hh^j(N,A)) \Rightarrow \hh^{i+j}(\Gamma,A)$, which in turn is an instance of the Grothendieck spectral sequence associated to the factorization of the $G$-invariants as $(-)^G \mc \begin{tikzcd}[cramped, ampersand replacement = \&] G\modcat \ar{r}[above]{(-)^N} \& G/N\modcat \ar{r}[above]{(-)^{G/N}} \& \abcat \end{tikzcd}$.
  }
}
  i.e. the following diagram is commutative with exact rows: 
  \[
    \begin{tikzcd}[column sep = small]
      0 
      \ar{r}
      &
      \hh^1(\Gamma,G)
      \ar{r}[above]{\mathrm{inf}}
      &
      \hh^1_{\mathrm{cont}}(\erig, G)
      \ar{r}[above]{\mathrm{res}}
      &
      \hh^1(u,G)^{\Gamma}
      \ar{r}[above]{\mathrm{trg}}
      &
      \hh^2(\Gamma,G)
      \\ 
      0 
      \ar{r}
      &
      \hh^1(\Gamma,G)
      \ar{r}
      \ar[equal]{u}
      &
      \hka* 
      \ar{r}
      \ar[hookrightarrow]{u}
      &
      \hom(u,Z)^{\Gamma}
      \ar[hookrightarrow]{u}
      \ar{r}
      &
      \ar[equal]{u}
      \hh^2(\Gamma,G)
    \end{tikzcd}
  \]
  where we ignore $\hh^2(\Gamma,G)$ if $G$ is not abelian.
\end{lem}

Note that we have the factorization over $\hom(u,Z)^{\Gamma}$ since the restricted action of $u$ is trivial.
\latercol{
Moreover, since for any continous cocycle $z\in \cocyc^1(u\to\erig,Z\to G)$, the image of $u(\bar{f})$ under $z$ in $G$ is contained in $\cen(G) = \ker(G \twoheadrightarrow \adj{G})$, we get a factorization in the following diagram 
\[
  \begin{tikzcd}[ampersand replacement=\&]
    u(\bar{F})
    \ar[hookrightarrow]{r}
    \ar{d}[left]{\restrict{z}{u}}
    \&
    \erig 
    \ar[twoheadrightarrow]{r}
    \ar{d}[right]{z}
    \&
    \Gamma
    \ar[dashed]{d}
    \\
    Z 
    \ar[hookrightarrow]{r}
    \&
    G 
    \ar[twoheadrightarrow]{r}
    \&
    \adj{G}
  \end{tikzcd}
\]
or, in other words, the image of $z$ in $\cocyc^1(\erig,\adj{G})$ belong to $\cocyc(\Gamma,\adj{G})$, and thus gives rise to an inner twist $G^z$ of $G$.
}
\begin{lem}
  The set $\hka*$ is finite.
\end{lem}
\begin{proof}
  We do this using the exact sequence from \cref{lem:inf-res}, which tells us that it is enough to see that $\hom(u,Z)^{\Gamma}$ is finite and that the map $f\mc \hka* \to \hom(u,Z)^{\Gamma}$ has finite fiber over its image.
  For the first statement, we use the presentation 
  \[
    \hom(u,Z)^{\Gamma} 
    = 
    \varinjlim \hom(u_{E/F,n},Z)^{\Gamma}
  \]
  \todocol{now the article says that since $Z$ is finite, each of the terms appearing in the filtered colimit are finite (which i understand), but this doesn't necessarily imply that the colimit is again finite, right?}
  For the second assertion, we use that for any $[z] \in \hka*$ with lift $z \in \cocyc(u \to \erig, Z \to G)$, the fiber over $f(z)$ is given by $\hh^1(\Gamma, G^z)$, where $G^z$ is the inner form of $G$ obtained from twisting by $z$ (for more on twisting, see e.g. \cite[Prop.35bis]{galoiscohomology} or \cite[§28]{involutions}).\footnote{\latercol{I think one can argue as follows: we know that the fiber of $\mathrm{res}$ over the distinguished element is given by $\hh^1(\Gamma,G)$, since the inflation-restriction sequence is an exact sequence of pointed sets.
  Now for any $[z]$ in $\hh^1(\Gamma,G)$ with corresponding twist $G^z$, there is an a commutative diagram of the form 
  \[
    \begin{tikzcd}[ampersand replacement = \&]
      \hh^1(\she,G^z)
      \ar{r}[above]{\rest_{G^z}}
      \&
      \hh^1(u,G^z)
      \\ 
      \hh^1(\she,G)
      \ar{r}[below]{\rest_G}
      \ar{u}[left]{\tau_z}
      \&
      \hh^1(u,G^z)
      \ar{u}[right]{\tau_z}
    \end{tikzcd},
  \]
  and $\tau_z$ induces a bijection between the fiber over $\mathrm{res}(z)$ and the kernel of $\overline{\mathrm{res}}$ (the main ingredient here is the explicit description of $\tau_z$, together with the fact that $u$ still acts  trivially on $G^z$).
  But  now, again by the inflation-restriction-sequence, this kernel is given precisesly by $\hh^1(\Gamma,G^z)$.
  }}
  It is then a general statement that $\hh^1(\Gamma,G)$ is finite for any algebraic group $G$ defined over the local field $F$ (c.f. \cite[Thm.6.14]{platonov}).
\end{proof}

\begin{theorem}
  \label{thm:for-split}
  Let $G$ be a connected reducitve group over $F$, let $Z$ be the center of $\der{G}$, and set $\bar{G} \defo G / Z$.
  Then there are natural surjections
  \[
    \begin{tikzcd}[column sep = 1.5em]
      \hka*
      \ar[twoheadrightarrow]{r}[above]{a}
      &
      \hh^1(\Gamma,\bar{G})
      \ar[twoheadrightarrow]{r}
      &
      \hh^1(\Gamma,\adj{G}).
    \end{tikzcd}
  \]
  Assume moreover that $G$ is split.
  \begin{enumerate}
    \item 
      If $F$ is $p$-adic, then both maps are bijective.
    \item 
      If $F = \rr$, then the second map is bijective and the first map has trivial kernel (but possibly non-trivial fibers away from the neutral element).
  \end{enumerate}
\end{theorem}

In particular, we have found a ``natural'' set that always surjects onto the set that classifies inner forms!

\begin{proof}
  Let us sketch the proof:
  For the map $\hh^1(\Gamma,\bar{G}) \to \hh^1(\Gamma,\adj{G})$ we use that we have a $\Gamma$-equivariant decomposition $\bar{G} = \adj{G} \times Z(G) / Z$, which induces a group isomorphism 
  \[
    \hh^1(\Gamma, \bar{G})
    \isomorphism 
    \hh^1(\Gamma,\adj{G})
    \times 
    \hh^1(\Gamma,Z(G)/Z),
  \]  
  and the map in question is the natural projection, which is surjective.
  If $G$ is split, then $Z(G)/G$ is a split torus which has vanishing first cohomology, so the second map bijective in this case.
  \par 
  For the surjectivity of $a$, one uses that it sits in the diagram 
  \begin{equation}
    \label{eq:commutative-inflation}
    \begin{tikzcd}[column sep = 1.5em]
      0 
      \ar{r}
      &
      \hh^1(\Gamma,G)
      \ar{r}[above]{\mathrm{inf}}
      \ar[equal]{d}
      &
      \hka*
      \ar{r}[above]{\mathrm{res}}
      \ar{d}[right]{a}
      &
      \hom(u,\Gamma)
      \ar{d}[right]{\xi^{\ast}}
      \\ 
      &
      \hh^1(\Gamma,G)
      \ar{r}
      &
      \hh^1(\Gamma,\bar{G})
      \ar{r}
      &
      \hh^2(\Gamma,Z)
    \end{tikzcd}
\end{equation}
  where $\xi$ is the element in $\hh^2(\Gamma,u)$ defining $\erig$, and $\xi^{\ast} \mc \hom(u,Z) \to \hh^2(\Gamma,Z)$ is given by $\phi \mapsto \phi(\xi)$.
  One can show that $\xi$ is surjective, using a version of the Tate-Nakayama isomorphism.
  In the abelian case, the diagram \eqref{eq:commutative-inflation} can then be extended to the right in each row by $\hh^2(\Gamma,G)$, and the four-lemma implies the claim.
  In the non-abelian case, the diagram \eqref{eq:commutative-inflation} does not suffice to conclude, but we can reduce to this case:
  Let $R \subset G$ be a Levi subgroup and $S\subset R$ a fundamental maximal torus (a maximal torus whose dimension of the split component is as small as possible).
  Then $Z \sse S$, and we a diagram of the form 
  \[
    \begin{tikzcd}
      \hka(u\to \erig, Z \to S)
      \ar{r}
      \ar{d}[left]{a}
      &
      \hka*
      \ar{d}
      \\ 
      \hh^1(\Gamma,\bar{S})
      \ar{r}
      &
      \hh^1(\Gamma,\bar{G})
    \end{tikzcd}
  \]
  We have seen above that the map $a\mc \hka(u\to \erig, Z \to S) \to \hh^1(\Gamma,\bar{S})$ is surjective.
  Moreover, $\bar{S} \sse \bar{R}$ is again a fundamental maximal torus, and in this case, the bottom-horizontal map $\hh^1(\Gamma,\bar{S}) \to \hh^1(\Gamma,\bar{G})$ is known to be surjective (\cite[Lem.10.2]{kottwitz-elliptic}).
  \par 
  Finally, lets argue why $a\mc \hka* \to \hh^1(\Gamma,\bar{G})$ has trivial kernel:
  We have already seen that the map $\hh^1(\Gamma,\bar{G}) \to \hh^1(\Gamma,\adj{G})$ is injective, so the kernel of $a$ agress with the kernel of the composition 
  \[
    \begin{tikzcd}[column sep = 1.5em]
      \hka*
      \ar{r}[above]{a}
      &
      \hh^1(\Gamma,\bar{G})
      \ar{r}
      &
      \hh^1(\Gamma,\adj{G}).
    \end{tikzcd}
  \]
  One then shows that this kernel coincides with the kernel of $\hh^1(\Gamma,G) \to \hh^1(\Gamma,\adj{G})$, which is now accesible via the long-exact sequence assoicated to $Z(G) \hookrightarrow G$, i.e. it is enough to see that $\adj{G} \to \hh^1(\Gamma,Z(G))$ is surjective.
  \todocol{For this, it does in fact suffice that $\adj{T}(F) \to \hh^1(\Gamma,Z(G))$ is surjective for $T \sse G$ a maximal torus, which follows from $\hh^1(\Gamma,\adj{G})$ being trivial.}
  Now finally, in the $p$-adic case, we know that $\hh^1(\Gamma,G) \to \hh^1(\Gamma,\adj{G})$ is in fact a group homomorphism, so the kernel being trivial implies that it is in fact injective.
\end{proof}

\section{Extending the Kottwitz isomorphism}
The goal now is to extend the morphism $\alpha\mc \hh^1(\Gamma,G) \to \pi_0(\cen(\widehat{G})^{\Gamma})^D$ to also obtain a more manageable description of $\hka*$.

\begin{construction}
  Recall that we have the category $\rcat$ consisting of maps $[Z \to G]$, where $G$ is a reductive group over $F$ and $Z$ is a finite multiplicative group. 
\end{construction}

\begin{theorem}
  There is an isomorphism 
  \[
    \iota \mc \yptor \isomorphism \hhab(u \to \erig)
  \]
  of functors $\rcat \to \setcat$, which lifts the morphism of functors $\yptor \to \hom(u,-)^{\Gamma}$.
  In the $p$-adic case, this functor is compatible with the Tate-Nakayama isormorphism in the sense that the diagram 
  \[
    \begin{tikzcd}
      \hh^1(\Gamma,G)
      \ar{r}[above]{\mathrm{inf}}
      \ar{d}[left]{\alpha}
      &
      \hh^1(u \to \erig, Z \to G)
      \ar{d}[right]{\sim}
      \\ 
      \pi_0(\cen(\hat{G})^{\Gamma})^D 
      \ar[hookrightarrow]{r}
      &
      \pi_0(\cen(\hat{\bar{G}})^+)^D
    \end{tikzcd}
  \]
  commutes for any finite central subgroup $Z$, where $\cen(\hat{\bar{G}})^+$ is the preimage of $\cen(\widehat{G})^{\Gamma}$ inside $\cen(\hat{\bar{G}})$ under the natural map $\hat{\bar{G}} \to \widehat{G}$.
\end{theorem}
We will not prove this theorem in the seminar talk, but let us at least sketch what is going on:
The first step is to construct a map in the case that the functors are restricted to $\tcat$ (i.e. the target in $[Z \to S]$ is a torus).
Already the construction of the map is difficult --- it involves the new notion of ``unbalanced cup products'' and a more explicit construction of the cocycle $\xi \in \hh^2(\Gamma,u)$ that gave rise to the gerbe $\erig$.
Let us at least give the construction of $\bar{Y}_{+,\mathrm{tor}}$ in the case of $[Z \to S] \in \tcat$: 
Here, we set $\bar{S} = S/Z$ and then $\bar{Y} \defo \cocha(\bar{S})$, $\bar{Y}_+ \defo \bar{Y} / I\bar{Y}$ for $I \sse \zz[\Gamma_{E/F}]$ with $E/F$ an extension that splits $S$ ($\bar{Y}_+$ is independent of this choice).
This already suggests compatibility with the classical Tate-Nakayama duality as reviewed in the begining of the talk.
Part of the proof is that we have the compatibility 
\[
  \begin{tikzcd}
    \hh^1(\Gamma,S)
    \ar{r}
    &
    \hh^1(u\to\erig,Z\to S)
    \\
    \frac{\cocha(S)}{I\cocha(S)}
    \ar{r}
    \ar{u}
    &
    {\bar{Y}_{+,\mathrm{tor}}([Z \to S])}
    \ar{u}
  \end{tikzcd}
\]
\begin{example}
  Let's continue \cref{example:special1}.
  Now $\der{\SL_n} = \SL_n$ which has center given by $\mu_n$.
  Recall that the Galois action on $\widehat{\SL_n}$ was trivial, so we have have $\cen(\hat{\bar{G}}) = \mu_n$, which recovers what we already knew from \cref{thm:for-split}.
\end{example}

\begin{rem}
  \todocol{It might be interesting to find an example where the unique quasi-split inner form of $G$ is not split, and where the surjection \[\hh^1(u\to \erig, \cen(\der{G}) \to G) \twoheadrightarrow \hh^1(\Gamma,\adj{G})\] from \cref{thm:for-split} has non-trivial fibers, but $\hh^1(\Gamma,G) \to \hh^1(\Gamma,\adj{G})$ is not surjective (so we can't take unitary groups).
  I couldn't find one.}
\end{rem}
\printbibliography
%\listoftheorems[ignoreall,
%show={todo, danger}]
\end{document}
